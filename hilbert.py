import sys, math
import numpy
import matplotlib.pyplot as plt

pointsX = []
pointsY = []
  
def hilbert(x0, y0, xi, xj, yi, yj, n):
    if n <= 0:
        X = x0 + (xi + yi)/2
        Y = y0 + (xj + yj)/2
        
        # Output the coordinates of the cv
        #print('%s %s 0' % (X, Y))
        pointsX.append(X)
        pointsY.append(Y)
        
    else:
        hilbert(x0,               y0,               yi/2, yj/2, xi/2, xj/2, n - 1)
        hilbert(x0 + xi/2,        y0 + xj/2,        xi/2, xj/2, yi/2, yj/2, n - 1)
        hilbert(x0 + xi/2 + yi/2, y0 + xj/2 + yj/2, xi/2, xj/2, yi/2, yj/2, n - 1)
        hilbert(x0 + xi/2 + yi,   y0 + xj/2 + yj,  -yi/2,-yj/2,-xi/2,-xj/2, n - 1)
        
def main():
    args = sys.stdin.readline()

    arg = args.split()
    # Get the inputs
    reps = int(arg[0])

    # Create the curve
    hilbert(0.0, 0.0, 1.0, 0.0, 0.0, 1.0, reps)
    
    fig = plt.figure()
    ax = fig.gca()
    #ax.set_xticks(numpy.arange(0.,1.,0.05))
    #ax.set_yticks(numpy.arange(0.,1.,0.05))
    #plt.grid()
    plt.plot(pointsX, pointsY)
    plt.show()

if __name__ == "__main__":
    main()
