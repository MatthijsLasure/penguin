Gnu95FCompiler instance properties:
  archiver        = ['/vsc-mounts/antwerpen-
                    apps/ivybridge/sl6/GCCcore/6.3.0/bin/gfortran', '-cr']
  compile_switch  = '-c'
  compiler_f77    = ['/vsc-mounts/antwerpen-
                    apps/ivybridge/sl6/GCCcore/6.3.0/bin/gfortran', '-Wall', '
                    -g', '-ffixed-form', '-fno-second-underscore', '-fPIC', '-
                    O3', '-funroll-loops']
  compiler_f90    = ['/vsc-mounts/antwerpen-
                    apps/ivybridge/sl6/GCCcore/6.3.0/bin/gfortran', '-Wall', '
                    -g', '-fno-second-underscore', '-fPIC', '-O3', '-funroll-
                    loops']
  compiler_fix    = ['/vsc-mounts/antwerpen-
                    apps/ivybridge/sl6/GCCcore/6.3.0/bin/gfortran', '-Wall', '
                    -g', '-ffixed-form', '-fno-second-underscore', '-Wall', '-
                    g', '-fno-second-underscore', '-fPIC', '-O3', '-funroll-
                    loops']
  libraries       = ['gfortran']
  library_dirs    = []
  linker_exe      = ['/vsc-mounts/antwerpen-
                    apps/ivybridge/sl6/GCCcore/6.3.0/bin/gfortran', '-Wall', '
                    -Wall']
  linker_so       = ['/vsc-mounts/antwerpen-
                    apps/ivybridge/sl6/GCCcore/6.3.0/bin/gfortran', '-Wall', '
                    -g', '-Wall', '-g', '-shared']
  object_switch   = '-o '
  ranlib          = ['/vsc-mounts/antwerpen-
                    apps/ivybridge/sl6/GCCcore/6.3.0/bin/gfortran']
  version         = LooseVersion ('6.3.0')
  version_cmd     = ['/vsc-mounts/antwerpen-
                    apps/ivybridge/sl6/GCCcore/6.3.0/bin/gfortran', '-
                    dumpversion']
IntelEM64TFCompiler instance properties:
  archiver        = ['/vsc-mounts/antwerpen-apps/noarch/intel-
                    psxe/2017_update2/compilers_and_libraries_2017.2.174/linux
                    /bin/intel64/ifort', '-cr']
  compile_switch  = '-c'
  compiler_f77    = ['/vsc-mounts/antwerpen-apps/noarch/intel-
                    psxe/2017_update2/compilers_and_libraries_2017.2.174/linux
                    /bin/intel64/ifort', '-FI', '-fPIC', '-openmp -fp-model
                    strict -O1', '-xSSE4.2']
  compiler_f90    = ['/vsc-mounts/antwerpen-apps/noarch/intel-
                    psxe/2017_update2/compilers_and_libraries_2017.2.174/linux
                    /bin/intel64/ifort', '-FR', '-fPIC', '-openmp -fp-model
                    strict -O1', '-xSSE4.2']
  compiler_fix    = ['/vsc-mounts/antwerpen-apps/noarch/intel-
                    psxe/2017_update2/compilers_and_libraries_2017.2.174/linux
                    /bin/intel64/ifort', '-FI', '-fPIC', '-openmp -fp-model
                    strict -O1', '-xSSE4.2']
  libraries       = []
  library_dirs    = []
  linker_exe      = None
  linker_so       = ['/vsc-mounts/antwerpen-apps/noarch/intel-
                    psxe/2017_update2/compilers_and_libraries_2017.2.174/linux
                    /bin/intel64/ifort', '-shared', '-shared', '-nofor_main']
  object_switch   = '-o '
  ranlib          = ['/vsc-mounts/antwerpen-apps/noarch/intel-
                    psxe/2017_update2/compilers_and_libraries_2017.2.174/linux
                    /bin/intel64/ifort']
  version         = LooseVersion ('17.0.2.174')
  version_cmd     = ['/vsc-mounts/antwerpen-apps/noarch/intel-
                    psxe/2017_update2/compilers_and_libraries_2017.2.174/linux
                    /bin/intel64/ifort', '-FI', '-V', '-c',
                    '/tmp/tmpwpglo4e3/5r469ugq.f', '-o',
                    '/tmp/tmpwpglo4e3/5r469ugq.o']
GnuFCompiler instance properties:
  archiver        = ['/usr/bin/g77', '-cr']
  compile_switch  = '-c'
  compiler_f77    = ['/usr/bin/g77', '-g', '-Wall', '-fno-second-
                    underscore', '-fPIC', '-O3', '-funroll-loops']
  compiler_f90    = None
  compiler_fix    = None
  libraries       = ['g2c']
  library_dirs    = []
  linker_exe      = ['/usr/bin/g77', '-g', '-Wall', '-g', '-Wall']
  linker_so       = ['/usr/bin/g77', '-g', '-Wall', '-g', '-Wall', '-
                    shared']
  object_switch   = '-o '
  ranlib          = ['/usr/bin/g77']
  version         = LooseVersion ('3.4.6')
  version_cmd     = ['/usr/bin/g77', '-dumpversion']
Fortran compilers found:
  --fcompiler=gnu      GNU Fortran 77 compiler (3.4.6)
  --fcompiler=gnu95    GNU Fortran 95 compiler (6.3.0)
  --fcompiler=intelem  Intel Fortran Compiler for 64-bit apps (17.0.2.174)
Compilers available for this platform, but not found:
  --fcompiler=absoft   Absoft Corp Fortran Compiler
  --fcompiler=compaq   Compaq Fortran Compiler
  --fcompiler=g95      G95 Fortran Compiler
  --fcompiler=intel    Intel Fortran Compiler for 32-bit apps
  --fcompiler=intele   Intel Fortran Compiler for Itanium apps
  --fcompiler=lahey    Lahey/Fujitsu Fortran 95 Compiler
  --fcompiler=nag      NAGWare Fortran 95 Compiler
  --fcompiler=pathf95  PathScale Fortran Compiler
  --fcompiler=pg       Portland Group Fortran Compiler
  --fcompiler=vast     Pacific-Sierra Research Fortran 90 Compiler
Compilers not available on this platform:
  --fcompiler=hpux      HP Fortran 90 Compiler
  --fcompiler=ibm       IBM XL Fortran Compiler
  --fcompiler=intelev   Intel Visual Fortran Compiler for Itanium apps
  --fcompiler=intelv    Intel Visual Fortran Compiler for 32-bit apps
  --fcompiler=intelvem  Intel Visual Fortran Compiler for 64-bit apps
  --fcompiler=mips      MIPSpro Fortran Compiler
  --fcompiler=none      Fake Fortran compiler
  --fcompiler=sun       Sun or Forte Fortran 95 Compiler
For compiler details, run 'config_fc --verbose' setup command.
Removing build directory /tmp/tmp5o43ik63
