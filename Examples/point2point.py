from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()

print('Hello world from rank {}'.format(rank))

if rank == 0:
	data = {'a': 7, 'b': 3.14}
	print('rank {} sending data={}'.format(rank,data))
	comm.send(data, dest=1, tag=11)
elif rank == 1:
	data = comm.recv(source=0, tag=11)
	print('Rank {} received data={}'.format(rank, data))
else:
	print('Rank {} doing nothing'.format(rank))
	try:
		print(data)
	except Exception as e:
		print(e)
		
