# Unit Test for my_module
import numpy
import my_module
import unittest

mMM = my_module.my_module

class UT_my_module(unittest.TestCase):
	def setUp(self):
		pass

	def test_sq(self):
		args = range(-2,3)
		for a in args:
			aa = mMM.sq(a)
			self.assertEqual(aa, a*a)
		args = numpy.arange(-5, 5, 0.25)
		for a in args:
			aa = mMM.sq(a)
			self.assertEqual(aa,a*a)

if __name__ == '__main__':
	unittest.main()
