#!/bin/bash

# So it finds the PenguinLover module
export PYTHONPATH=$PYTHONPATH:$VSC_HOME/git/penguin/PenguinLover/PenguinLover/build/

opre=FTL_out_
epre=FTL_err_

rm -f data/$opre*.txt
rm -f data/$epre*.txt


python src/FollowTheLeader.py -l 2
#mpiexec -np=4 --outfile-pattern=data/$opre%r_%g_%h.txt --errfile-pattern=data/$epre%r_%g_%h.txt python src/FollowTheLeader.py -l 2
