#!/bin/bash
#PBS -l nodes=1:ppn=10,walltime=00:30:00 -A lp_a_parallelprogramming -N Snejk

module load Python
module load intel

cd $PBS_O_WORKDIR


# So it finds the PenguinLover module
export PYTHONPATH=$PYTHONPATH:$VSC_HOME/git/penguin/PenguinLover/PenguinLover/build/

# Prefixes for output
outfolder=data/half/
opre=$outfolder/FTL_out_
epre=$outfolder/FTL_err

if [[ ! -d "$outfolder" ]]; then mkdir $outfolder; fi

# Remove existing output files
rm -f $opre*.txt
rm -f $epre*.txt

# Execute order 66
mpiexec -np=$PBS_NP --outfile-pattern=$opre%r_%g_%h.txt --errfile-pattern=$epre%r_%g_%h.txt -hostfile $PBS_NODEFILE python src/FollowTheLeader.py
