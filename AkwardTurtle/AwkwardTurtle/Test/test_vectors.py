"""
Geschreven door Roy
Om na te kijken of de dimensies en de waarden inderdaad klopte bij de gebruikte bewerkingen bij de functie raccentcalc
"""

import numpy as np
import math


Rcut = 5
index = 28
scale = 3.50930
Na = 28

latvecA = np.array([-0.5, 0.5, 0.5])
latvecB = np.array([0.5, -0.5, 0.5])
latvecC = np.array([0.5, 0.5, -0.5])
print('latvec size: {}'.format(np.size(latvecA,0)))

i = index * (latvecA * scale / Na)
j = index * (latvecB * scale / Na)
k = index * (latvecC * scale / Na)
print('i size: {}'.format(np.size(i,0)))
print('i j k: {} {} {}'.format(i, j, k))

pos =  i + j + k
print('pos size: {}'.format(np.size(pos,0)))
print('pos = {}'.format(pos))

# Hier is zoals we het hebben gedaan in de functie raccencalc
posdot = np.dot(pos,pos)
print('posdot size: {}'.format(np.size(posdot)))
print('posdot = {}'.format(posdot))

# OK het klopt! 

"""
VERBETERINGSVOORSTEL VOOR DE FORTRANCODE PINKPANDA en RACCENTCALC in PYTHON

pos = vecA * arraysIJK[num, 0] + vecB * arraysIJK[num, 1] + vecC * arraysIJK[num, 2]
Vervangen door
pos = arraysIJK[num, :] * gridVec        (1x3).(3x3)
Dit zou hetzelfde moeten geven en bespaart een aantal bewerkingen
"""
a = np.array([[1],
              [2]])

print(np.size(a,0))
print(np.size(a,1))

b = np.array([[3],
              [4]])

c = np.array([[5],
              [6]])

conc = np.concatenate((a,b,c),axis=1)

print(conc)
print(np.size(conc,0))
print(np.size(conc,1))
