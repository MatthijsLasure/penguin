'''
Created on May 9, 2017

@author: vsc20328
'''

import numpy as np
import matplotlib.pyplot as plt

filename="integraal.txt"

lines = sum(1 for line in open(filename))
integral = np.empty(lines)
derivative = np.empty(lines)

dintegral = np.empty(lines)
dderivative = np.empty(lines)

with open(filename) as f:
    for l, line in enumerate(f):
        words = line.split()
        values = [float(w) for w in words[0:2]]
         
#         integral[l] = values[2]
#         derivative[l] = values[3]
        dintegral[l] = values[0]
        dderivative[l] = values[1]

ax = plt.gca()
ax2 = ax.twinx()
ax.plot(dintegral)
ax2.plot(dderivative,'r')
plt.show()


# ax = plt.gca()
# ax2 = ax.twinx()
# ax.plot(dintegral, '.')
# ax2.plot(dderivative)
# plt.show()

if __name__ == '__main__':
    pass