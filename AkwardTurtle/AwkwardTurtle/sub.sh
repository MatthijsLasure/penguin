#!/bin/bash
#PBS -l nodes=1:ppn=20,walltime=00:15:00 -A lp_a_parallelprogramming -N Snejk

module load Python
module load intel

cd $PBS_O_WORKDIR

src=$VSC_HOME/git/penguin/AkwardTurtle/AwkwardTurtle/src

# So it finds the PenguinLover module
export PYTHONPATH=$PYTHONPATH:$VSC_HOME/git/penguin/PenguinLover/PenguinLover/build/

# Prefixes for output
outfolder=$PBS_WORKDIR
opre=FTL_out_
epre=FTL_err

#if [[ ! -d "$outfolder" ]]; then mkdir $outfolder; fi

# Remove existing output files
rm -f $opre*.txt
rm -f $epre*.txt

# Execute order 66
mpiexec -np=$PBS_NP --outfile-pattern=$opre%r_%g_%h.txt --errfile-pattern=$epre%r_%g_%h.txt -hostfile $PBS_NODEFILE python $src/FollowTheLeader.py
