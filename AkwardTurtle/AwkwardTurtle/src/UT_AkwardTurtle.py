'''
Created on May 3, 2017

@author: vsc20328 & SNEJK

https://docs.python.org/3/library/unittest.html
'''
import unittest
import numpy as np
import math
from datetime import datetime as dt

import inputdata
import PenguinLover
import rootFinder
import gettingStarted
import CutTheSnejk

get_intderv = PenguinLover.integralderivative.get_int_derv 


class Test(unittest.TestCase):

    @classmethod
    def setUpClass(self):
        self.Rc = CutTheSnejk.cutOff.findRc(None, 1.0)
        #self.Rc = 10

    def tearDown(self):
        pass
    
    # FollowTheLeader tests
    def test_FollowTheLeader(self):
        print("Hellos")
        pass
    
    def test_000_checkPrecision(self):
        what = PenguinLover.integralderivative.getprec()
        print("PenguinLover reports {}".format(what))
        
    # InputData tests
    def test_001_InputData(self):
        print('{} > UT > InputData'.format(dt.now()))
        filename = '../data/CHGCAR_Li_LDA_exp.txt'
        for keep_linear_n in [False,True]:
            with self.subTest(keep_linear_n = keep_linear_n):
                input_data = inputdata.InputData(filename,keep_linear_n=keep_linear_n)
                self.assertEqual(input_data.material, 'Li')
                self.assertEqual(input_data.scale, 3.5093000000000001)
                self.assertTrue(np.all(np.abs(input_data.lattice_vectors) == 0.5))
                for i in range(3):
                    self.assertLess(input_data.lattice_vectors[i,i],0)
                self.assertEqual(input_data.Na, 28)
                self.assertEqual(input_data.Nb, 28)
                self.assertEqual(input_data.Nc, 28)
                if keep_linear_n:
                    self.assertEqual(input_data.n.size, 28**3)
                    self.assertAlmostEqual(input_data.n[ 0], 0.32007229926E+00)
                    self.assertAlmostEqual(input_data.n[15], 0.10660938991E+01)
                    self.assertAlmostEqual(input_data.n[30], 0.37554484787E+00)
                    self.assertAlmostEqual(input_data.n[-1], 0.33581189662E+00)

                    self.assertTrue(np.all(input_data.n[0:5]==input_data.nijk[0:5,0,0]))
                    self.assertTrue(np.all(input_data.n[28+0:28+5]==input_data.nijk[0:5,1,0]))
                else:
                    self.assertEqual(input_data.nijk.size, 28**3)
                    self.assertAlmostEqual(input_data.nijk[ 0, 0, 0], 0.32007229926E+00)
                    self.assertAlmostEqual(input_data.nijk[15, 0, 0], 0.10660938991E+01)
                    self.assertAlmostEqual(input_data.nijk[ 2, 1, 0], 0.37554484787E+00)
                    self.assertAlmostEqual(input_data.nijk[27,27,27], 0.33581189662E+00)
                #this is only true if the crystal has a cubic unit_cell with Z=2
                self.assertAlmostEqual(input_data.unit_cell_volume, 0.5*input_data.scale**3)
        Test.data = input_data
                       
        
                
    def test_002_GettingStarted(self):
        #self.skipTest("LOLOLO")
        print('{} > UT > GettingStarted'.format(dt.now()))

        d = Test.data
        setup = gettingStarted.setup(d, self.Rc)
        #IJK grootte checken 
        arraysIJK = setup.IJK
        sizeIJK = len(arraysIJK)
        print(sizeIJK)
        #groe array groot genoeg 
        
        #araay gesorteed
        def isSorted(x, key = lambda x: x): return all([key(x[i]) <= key(x[i + 1]) for i in range(len(x) - 1)])
        #n-sorted groot gnoeg
        #uit grote N paaar waarden selecteren; vergelijken met overeenkomt dat je verwacht
        
        
        Test.setup = setup
        
        #TODO: checks
        # Roy: zo stond het in de compute_a
        print(setup.IJK[1738,0], setup.IJK[1738,1], setup.IJK[1738,2])
        print(d.nijk[23,23,26])
        #print(setup.n[1738])
        
    def test_003_CutTheSnejk(self):
        co = CutTheSnejk.cutOff(Test.setup.raccent)
        
        Test.cutoff = co

    # Integraal & afgeleide tests
    def test_004_PenguinLover(self):
        #self.skipTest("yolo")
        print('{} > UT > PenguinLover'.format(dt.now()))

        
        data = Test.data
        s = Test.setup
        c = Test.cutoff

        a = 3.0        
        POS_RCUT = c.findIndex(a)
        p = [17, 15, 10]
        
        n = Test.setup.doNAssign(p, data.nijk)

        # I don't knnow how this stuff works...
        # But it does :/
        integral = 1.0
        derivative = 2.0
        
        integral, derivative = get_intderv(n,s.raccent, a, s.vol, POS_RCUT, integral, derivative)
        
        print('get_int_derv')
        print(integral, derivative)

        self.assertAlmostEqual(integral, -0.0431366240557,2)
        self.assertAlmostEqual(derivative, -0.9454247811064285)
        
        print('{} > UT > PenguinLover > Exiting'.format(dt.now()))
    
    #Testing Da coolest stuff ever!    
    def test_005_NewtonRaphson(self):
        print('{} > UT > Newton Raphson'.format(dt.now()))
        
        data = Test.data
        s = Test.setup
        c = Test.cutoff        
        
        a = 3.0
        POS_RCUT = c.findIndex(a)
        p = [0, 0, 0]
        
        n = Test.setup.doNAssign(p, data.nijk)

        
        a, step = rootFinder.NewtonRaphson(n, s.raccent, s.vol, c.m, c.b, guess=a, maxSteps=20)
        
        print('Get a')
        print(a, step)
        
        self.assertAlmostEqual(a, 2.80173691913898)
        self.assertEqual(step, 6, "NR steps!")


if __name__ == "__main__":
    unittest.main()