'''
Created on May 2, 2017

@author: vsc20328

Hoofdbestand vooor het programma: stuurt alles aan.
'''

#Follow the leader leader follow the leader

# Bestaande modules
import numpy as np
import matplotlib.pyplot as plt
from datetime import datetime as dt
from timeit import default_timer as timer
from mpi4py import MPI
import optparse

# Eigen modules
import inputdata # Data importeren
import rootFinder # Root Finders module
import gettingStarted # Setup module
import CutTheSnejk # Determine cutoff module
from numpy import single

#==============================================================================================================
#==============================================================================================================
#==============================================================================================================

"""
Read arguments from command line
"""
parser = optparse.OptionParser()

parser.add_option('-i',dest='inputName',help='Input file', default='data/CHGCAR_Li_LDA_exp.txt')
parser.add_option('-o',dest='outputName',help='Output file', default='data/output.txt')
parser.add_option('-d',dest='debug',help='Super debug info', default=False)
parser.add_option('-v',dest='verbose',help='Verbose info print', default=False)
parser.add_option('-l',dest='length', help='Custom p size (^3)', default=None)

options, args = parser.parse_args()

# Set stuff
_debug = options.debug
_debug = True

# Load everything once of many times?
singleLoad = True

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
numproc = comm.Get_size()

if rank == 0:
    with open("data/secret") as f:
        for l,line in enumerate(f):
            print(line[:-1])
    print("Snejk presents: Awkward turtle")
    
print("Hi, I'm thread {} out of {}!".format(rank, numproc))
print("{} > FollowTheLeader > Start".format(dt.now()))

"""
If only the master thread has to load the file, check that it is actually the master thread
"""
if (singleLoad and rank == 0) or not singleLoad:
   
    print("{} > FollowTheLeader > Command & Control".format(dt.now()))
    
    inputName = options.inputName
    outputName = options.outputName
    
    # Read in data
    data = inputdata.InputData(inputName, keep_linear_n=True)
    
    # Find a good cutoff: worst case scenario a=1.0
    RcInit = CutTheSnejk.cutOff.findRc(None, 1.0)
    
    
    # Process the input data
    s = gettingStarted.setup(data, Rc=RcInit)
    
    # Prepare the cutoff values
    cutoff = CutTheSnejk.cutOff(s.raccent)
    cutoffSlope = cutoff.m
    cutoffIntercept = cutoff.b
    
    print("{} > FollowTheLeader > Preparing p".format(dt.now()))
    
    # Make a list of p
    if options.length == None:
        ppreA = np.arange(data.Na)
        ppreB = np.arange(data.Nb)
        ppreC = np.arange(data.Nc)
    else: # Custom length if given om CMD
        l = int(options.length)
        ppreA = np.arange(l)
        ppreB = np.arange(l)
        ppreC = np.arange(l)
    
    
    p = gettingStarted.cartesian((ppreA, ppreB, ppreC))
    pnum = np.size(p,0)
    
else:
    # Prepare these variables, as they will be filled with the bcast
    data = None
    s = None
    cutoffSlope = None
    cutoffIntercept = None
    p = None
    
# Initial guess
thisa = 1.0

# If rank is 0, make a list of who does what, then distribute
if rank == 0:   
    plist = np.arange(pnum, dtype=np.int64) 
    ixDistr = np.array_split(plist, numproc)
    
    print("{} > FollowTheLeader > Command > Will calculate {} p's".format(dt.now(), pnum))
    
    # Range for rank 0
    ix = ixDistr[0];
    
    print("Rank 0 sending list!")
    
    for i in range(1,numproc):
        comm.send(ixDistr[i], dest=i, tag=1)
    
else: # Wait for number 0
    ix = comm.recv(source=0, tag=1)

print("{} > FollowTheLeader > Rank {} received list! {} {}".format(dt.now(), rank, ix[0], ix[-1]))


# If rank is 0, send the setup. Else, wait for it
if singleLoad:
    print("{} > FollowTheLeader > Exchanging data".format(dt.now()))
    data = comm.bcast(data, root=0)
    s = comm.bcast(s, root=0)
    cutoffSlope = comm.bcast(cutoffSlope, root=0)
    cutoffIntercept = comm.bcast(cutoffIntercept, root=0)
    p = comm.bcast(p, root=0)
    print("{} > FollowTheLeader > Done exchanging data".format(dt.now()))
    
print("Start @ {} {} {}".format(p[ix[0],0], p[ix[0],1], p[ix[0],2]))
print("Stop  @ {} {} {}".format(p[ix[-1],0], p[ix[-1],1], p[ix[-1],2]))
#============================================================================================
"""
Actual stuff
"""
print("{} > FollowTheLeader > Starting calculations".format(dt.now()))

a = np.empty((np.size(p,0),))
# For all p values
for i in ix:
    # Timer
    start = timer()
    
    # Maak n
    n = s.doNAssign(p[i], data.nijk)
    
    # Start NR
    # Recycle last a as this may be already pretty good
    thisa, step = rootFinder.NewtonRaphson(n, s.raccent, s.vol, cutoffSlope, cutoffIntercept, guess=thisa, maxSteps=20, _debug=_debug)
    #TODO: replace s.POS_RCUT with cutoffSlope
    
    # Opslaan
    a[i] = thisa
    
    # Print info
    end = timer()
    print("Cycle %3i %3i %3i %8.6f %2i %8.6f" % (p[i,0], p[i,1], p[i,2], thisa, step, end-start))
    
print("{} > FollowTheLeader > Calculations done".format(dt.now()))   
"""
Phone home
"""
# a comes back as a list, because f you
print("{} > FollowTheLeader > Gather".format(dt.now())) 
#finalaList = comm.gather(a, root=0)
finalaList = comm.gather(a[ix], root=0)
print("{} > FollowTheLeader > Gather done".format(dt.now())) 

# Save file on rank=0
if rank == 0:
    
    aa = np.empty((pnum,))
    # Concate list into a nice array
    for i in range(numproc):
    # Sounds like crazy talk
        #aa[ixDistr[i]] = finalaList[i][ixDistr[i]]
        aa[ixDistr[i]] = finalaList[i]
        
    
    print("{} > FollowTheLeader > Saving".format(dt.now()))
    with open(outputName, 'w') as f:
        f.write("%3s %3s %3s %13s\n" % ('pi', 'pj', 'pk', 'a'))
        for i in range(pnum):
            f.write("%3i %3i %3i %+e\n" % (p[i,0], p[i,1], p[i,2], aa[i]))
            
    print("{} > FollowTheLeader > Done saving".format(dt.now()))
    
    
print("{} > FollowTheLeader > The end!".format(dt.now()))
