"""
// programming assignment 2017
read_input.py

(c) (Engel)Bert Thijskens
"""
import numpy as np
from datetime import datetime as dt

#===============================================================================            
class InputData:
    """
    """
    def __init__(self,filename,keep_linear_n=False):
        """
        """
        print("{} > InputData > Entering".format(dt.now()))
        
        with open(filename) as f:
            self.lattice_vectors = np.empty((3,3),dtype=np.float) 
            begin_n = False
            for l,line in enumerate(f):
                line = line[:-1]
                if l>10:
                    if l <= 10+n_lines:
                        words = line.split()
                        values = [float(w) for w in words]
                        nvalues = len(values)
                        self.n[i:i+nvalues] = values
                        i += nvalues
                else:
                    if l==0:
                        self.material = line.strip()
                    elif l==1:
                        self.scale = float(line)
                    elif 2<=l<=4:
                        words = line.split()
                        for j,w in enumerate(words):
                            self.lattice_vectors[l-2,j] = float(w)
                    elif not line:
                        begin_n = True
                    elif begin_n:
                        words = line.split()
                        self.Na = int(words[0])
                        self.Nb = int(words[1])
                        self.Nc = int(words[2])
                        N = self.Na*self.Nb*self.Nc
                        self.n = np.empty((N,),dtype=np.float)
                        i = 0
                        n_lines = N//5
                        if N%5:
                            n_lines += 1
        self.nijk = self.n.reshape( (self.Na,self.Nb,self.Nc),order='F' )
        if not keep_linear_n:
            # the linear array is no longer needed
            del self.n
        self.unit_cell_volume = np.dot(self.lattice_vectors[0,:]
                                      ,np.cross(self.lattice_vectors[1,:],self.lattice_vectors[2,:])
                                      ) * (self.scale**3)
        print("{} > InputData > Exiting".format(dt.now()))
    #---------------------------------------------------------------------------
