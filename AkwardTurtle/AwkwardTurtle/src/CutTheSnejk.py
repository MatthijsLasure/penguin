'''
Created on May 16, 2017

@author: vsc20328
'''


from datetime import datetime as dt
import numpy as np
from scipy import stats

class cutOff():
    
    def __init__(self, raccent, lower=1, higher=20, interval=0.1):
        print("{} > CutTheSnejk > Entering".format(dt.now()))
        self.low = lower
        self.up = higher
        self.interval = interval
        
        self.a = np.arange(lower,higher, interval)
        
        print("{} > CutTheSnejk > makeList".format(dt.now()))        
        self.Rc = self.makeList(self.a)

        print("{} > CutTheSnejk > makeIndex".format(dt.now()))
        self.ix = self.makeIndex(raccent)

        print("{} > CutTheSnejk > Fit".format(dt.now()))
        slope, intercept, r_value, p_value, std_err = stats.linregress(self.a ** -3,self.ix)
        
        self.m = slope
        self.b = intercept
        
        print("Fit: Rc = m*a**-3 + b")
        print("m = {}, b = {}, R² = {}".format(slope, intercept, r_value))
                
        print("{} > CutTheSnejk > Exiting".format(dt.now()))
        
        
    def findRc(self, a, rest=1e-7, margin=0.001):
        """
        findRc: find the cutoffvalue for a given a
        :param a: the a for which the calculation takes place
        :param rest: the error that can remain on the used function (NR error)
        :param margin: the error that can remain on the real function (not used portion)
        """
        r = 2.5/a
        step = 0
        f = 1000
        
        while abs(f) > margin:
            ar  = a*r
            ar2 = ar*ar
            e = np.exp(-ar)
            f    =  ( 0.5*ar2 +   ar + 1 ) * e - rest
            df   = -( 0.5*ar2 + 2*ar + 2 ) * a * e
            r -= f/df
            step += 1
            
            
            if step > 50:
                break
             
        return r
    
    def makeList(self, a):
        """
        makeList: compile a list that correlates the a to the correct cutoffvalue
        :param a: a list of a's
        """
        Rc = np.empty(np.size(a,0))
        
        for i in range(len(a)):
            Rc[i] = self.findRc(a[i])
            
        return Rc
    
    
    # Legacy
    def makeIndex(self, raccent):
        """
        makeIndex: make a index that correlates an a to an index in the raccent
        :param Rc: list of cutoff values from makeList
        :param raccent: list of distances, relative from p
        """
        
        ix = np.empty_like(self.Rc)
        #ix2 = np.empty_like(self.Rc)
        
        """
        SearchSorted: Find indices where elements should be inserted to maintain order.
        This uses binary find, so it may underperform with SIMD
        https://docs.scipy.org/doc/numpy-1.10.4/reference/generated/numpy.searchsorted.html
        """
        
        for i in range(np.size(self.Rc,0)):
            j= np.searchsorted(raccent, self.Rc[i], side='right')
            ix[i] = j
        
        # Voor alle Rc
        # Legacy
#         for i in range(np.size(self.Rc,0)):
#             # Loop door raccent (gesorteerd :))
#             ix2[i] = 1000
#             for j in range(np.size(raccent,0)):
#                 if raccent[j] >= self.Rc[i]: # Als de waarde in r' > Rc, sla op en break
#                     ix2[i] = j
#                     break
            
        
        return ix
         
    def findIndex(self, a):
        ia = (a - self.low) / self.interval
        ia = int(ia)
        
        ix0 = self.ix[ia]
        ix1 = self.ix[ia+1]
        a0 = self.a[ia]
        
        dix = ix1 - ix0
        da = self.interval
        
        ix = ix0 + dix/da * (a - a0)

        return int(ix)           

if __name__ == '__main__':
    import matplotlib.pyplot as plt
    import inputdata
    import gettingStarted
    

    
    filename = '../data/CHGCAR_Li_LDA_exp.txt'
    id = inputdata.InputData(filename, keep_linear_n=True)
    
    top = cutOff.findRc(None,1)
    
    s = gettingStarted.setup(id, top)

    theCutOff = cutOff(s.raccent)
    
    slope, intercept, r_value, p_value, std_err = stats.linregress(theCutOff.a**-3, theCutOff.ix)
    
    print(slope, intercept, r_value)
    
    ixFit = theCutOff.a ** -3 * slope
    
    print("R² is {}".format(r_value))
    
    ax = plt.gca()
#     ax2 = plt.twinx()
#     ax.plot(theCutOff.a**-3, theCutOff.ix)
    ax.plot(theCutOff.a, theCutOff.ix, 'x', theCutOff.a, ixFit)
    plt.xlabel('a')
    plt.ylabel('Cutoff index')
#     ax2.plot(theCutOff.a, theCutOff.ix,'r')
    plt.show()
    pass