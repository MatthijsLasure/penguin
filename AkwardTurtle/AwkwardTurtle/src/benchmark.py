"""
Benchmark: zie hoe snel dingen gaan
"""

# Bestaande modules
import numpy as np
import matplotlib.pyplot as plt
import time
import math
from datetime import datetime as dt
from math import exp,sqrt
from timeit import default_timer as timer

# Eigen modules
import inputdata # Data importeren
import rootFinder # Root Finders module
import gettingStarted # Setup module
import PenguinLover
import CutTheSnejk

get_intderv = PenguinLover.integralderivative.get_int_derv

def bruteforce(input_data,p,cutoff_radius,a=1.0,verbose=False):
    """
    Compute integral and derivative for |R+r-p| < cutoff_radius
    :param input_data: an InputData object
    :param p: vector p
    :param cutoff_radius: cut-off radius
    :param float a: current a value
    """
    start = timer()
    
    au = input_data.lattice_vectors[0,:]*input_data.scale
    bu = input_data.lattice_vectors[1,:]*input_data.scale
    cu = input_data.lattice_vectors[2,:]*input_data.scale
    Na = input_data.Na
    Nb = input_data.Nb
    Nc = input_data.Nc
    ag = au/Na
    bg = bu/Nb
    cg = cu/Nc

    nR = 5
    nijk = input_data.nijk
#     if cutoff_radius is None:
        
    radius2 = cutoff_radius*cutoff_radius
    f_a_p = 0.0
    df_da = 0.0
    if verbose:
        print("I J K i j k f df delta_f delta_df")
    for I in range(-nR,nR+1):
        for J in range(-nR,nR+1):
            for K in range(-nR,nR+1):
                R = I*au+J*bu+K*cu
                for k in range(Nc):
                    for j in range(Nb):
                        for i in range(Na):
                            r = R   +(i-p[0])*ag \
                                    +(j-p[1])*bg \
                                    +(k-p[2])*cg
                            rr = np.dot(r,r)
                            if rr<=radius2:
                                r = sqrt(rr)
                                n = nijk[i,j,k]
                                e = exp(-a*r)
                                ne = n*e
                                f_a_p += ne
                                df_da += -r*ne
                                if verbose:
                                    #print(I,J,K,i,j,k,f_a_p,df_da,ne,-r*ne)
                                    print(r)
    Vijk = input_data.unit_cell_volume/(Na*Nb*Nc)
    f_a_p *= Vijk
    f_a_p -= 1.0
    df_da *= Vijk

    end = timer()
    
    return f_a_p,df_da, (end-start)   


print("{} > Benchmark > Start".format(dt.now()))

filename = 'data/CHGCAR_Li_LDA_exp.txt'
 
data = inputdata.InputData(filename, keep_linear_n=True)

RcInit = CutTheSnejk.cutOff.findRc(None, 1.0)
s = gettingStarted.setup(data, Rc=RcInit)

# Prepare the cutoff values
cutoff = CutTheSnejk.cutOff(s.raccent)
cutoffSlope = cutoff.m
cutoffIntercept = cutoff.b

#==============================================================================



a = np.arange(1,6,0.1)

i1 = np.empty((np.size(a),))
i2 = np.empty((np.size(a),))
i3 = np.empty((np.size(a),))

d1 = np.empty((np.size(a),))
d2 = np.empty((np.size(a),))
d3 = np.empty((np.size(a),))

# I don't knnow how this stuff works...
# But it does :/
integral = 1.0
derivative = 2.0

p = [17, 15, 10]

n = s.doNAssign(p, data.nijk)

POS_RCUT = min(cutoffSlope + cutoffIntercept, np.size(s.raccent))

#i, d = get_intderv(n,s.raccent, 1, s.vol, POS_RCUT, integral, derivative)
#print(i, d)
#exit()
i=0

a = 3.0
POS_RCUT_t = min(cutoffSlope * a**-3 + cutoffIntercept, np.size(s.raccent))

print("Go! {}".format(dt.now()))
i2[i], d2[i], t = bruteforce(data,p, POS_RCUT_t,a=a,verbose=False)
start2 = timer()
i3[i], d3[i] = get_intderv(n,s.raccent, a, s.vol, POS_RCUT_t, integral, derivative)
end2 = timer()
print(a, i2[i], i3[i], d2[i], d3[i], t, end2 - start2)

exit()

#==============================================================================
print("{} > Benchmark > Start run".format(dt.now()))
print("start\t{}".format(time.time()))
for i in range(np.size(a)):
    i1[i], d1[i] = get_intderv(n,s.raccent, a[i], s.vol, POS_RCUT, integral, derivative)
    
    start1 = timer()
    POS_RCUT_t = min(cutoffSlope * a[i]**-3 + cutoffIntercept, np.size(s.raccent))
    end1 = timer()
    i2[i], d2[i], t = bruteforce(data,p, POS_RCUT_t,a=a[i],verbose=False)
    start2 = timer()
    i3[i], d3[i] = get_intderv(n,s.raccent, a[i], s.vol, POS_RCUT_t, integral, derivative)
    end2 = timer()
    print(a[i], i1[i], i2[i], i3[i], d1[i], d2[i], d3[i], end1-start1, t, end2 - start2)

print("end\t{}".format(time.time()))
print("{} > Benchmark > End run".format(dt.now()))

plt.plot(a, i1, a, i2, a, i3)
plt.show()
    