'''
Created on May 2, 2017

@author: vsc20328 & SNEJK_Claudia

'''

#importeer de benodigde modules
from datetime import datetime as dt
from timeit import default_timer as timer

import PenguinLover
import numpy as np
from numpy import NaN

get_intderv = PenguinLover.integralderivative.get_int_derv 

# Defineer de functie om zo via de Newton-Raphson methode het nulpunt te bepalen en daaruit de a
# Met n = 
# raccent = 
# Vol = het volume
# icut = de cut-off
# guess = de initiële gok van het nulpunt
# tol =de tolerantie
# maxSteps = maximum aantal stappen dat de while loop mag doen 
# positie cutoff = cutoffslope/a

def NewtonRaphson(n, raccent, vol, cutoffslope, cutoffintercept, guess=3., tol=1e-8, maxSteps=20, _debug=False):
    # We beginnen bij steo 1
    step = 1
    #arbitraire namen om de functie cool te maken
    # We kiezen ranom waarden puur voor de functie en de loop
    integral = 10000
    derivative = 123456
    
    # We stellen a gelijk aan de gok; dus de waarde waarmee we beginnen  
    a = guess
    
    if _debug:
        print("{} > Newton Raphson > Starting Loop".format(dt.now()))
        print("%3s %13s %13s %13s %13s %13s %13s" % ('i','a', 'newa','f','df','posRcut','tijd'))

    while abs(integral) > tol:
        
        start = timer()
        posRCut= int(a**-3 * cutoffslope + cutoffintercept)
        posRCut = min(posRCut, np.size(raccent,0))
        #posRCut = 8708675
        #print(posRCut)
        # Bereken integraal & afgeleide
        integral, derivative = get_intderv(n,raccent,a, vol, posRCut, integral, derivative)
        
        end = timer()
        
        newa = a - integral / derivative
        
        if _debug:
            print("%3i %+e %+e %+e %+e %13i %+e" % (step,a, newa,integral,derivative, posRCut, end-start))
          
        # Bereken nieuwe a
        # Vergeet niet: trek 1 af van integraal!
        # a = a - (I - 1) / D
        
        # Verhoog counter
        step += 1
        #a = min(abs(newa),500)
        a = newa
        if (step > maxSteps):
            try:
                raise Exception('NR did not converge! p={}, a={}, val={}.'.format(a, integral))
            except:
                pass
            return NaN
            break
    if _debug:
        print("{} > Newton Raphson > Exiting Loop".format(dt.now()))  
#     plt.plot(arrayS, arrayA, arrayS, arrayI)#, arrayS, arrayD)
#     plt.legend(labels=['a','Int'])
#     plt.show()
    return a, step
