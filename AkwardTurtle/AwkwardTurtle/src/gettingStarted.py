"""
Created on May 6, 2017

@author: vsc20328
"""

import numpy as np
import math
from datetime import datetime as dt
from PenguinLover import pinkpanda as pp

# Debug
# import ctypes
# import matplotlib.pyplot as plt


class setup(object):
    """
    Dit is de setup class.
    Code van Roy (zie test_PP > module_setup en compute_a)
    Omgezet door Matthijs naar 1 class voor de eenvoud
    """


    def __init__(self, data, Rc=None):
        """
        Constructor
        Origineel uit compute_a
        """
        
        # Debug
        from PenguinLover import integralderivative as ida
        print(ida.getprec())
        # Say hi
        print("{} > GettingStarted > Entering".format(dt.now()))
        
        print("The cutoff is set at {}".format(Rc))
        
        # 1) Defining the cut-off value
        #Rc = cutoffchoice()
#         if Rc==None:
#             Rc = cutoffchoice()
        
        # 2) Generate arrays for input cartesian function
        
        magnA = math.sqrt(sum(data.lattice_vectors[0,:]**2))*data.scale
        magnB = math.sqrt(sum(data.lattice_vectors[1,:]**2))*data.scale
        magnC = math.sqrt(sum(data.lattice_vectors[2,:]**2))*data.scale
        
        NboxA = math.ceil(Rc/magnA)
        NboxB = math.ceil(Rc/magnB)
        NboxC = math.ceil(Rc/magnC)

        
        # Calculate the intergrid distance
        self.gridA = magnA / data.Na
        self.gridB = magnB / data.Nb
        self.gridC = magnC / data.Nc
        
        # Calculate vol
        
        volTotaal = data.unit_cell_volume
        vol = volTotaal / (data.Na*data.Nb*data.Nc)
        
        # Calculate mini vectors: per gridpoint
        self.gridVec = np.empty((3,3))
        self.gridVec[0,:] = data.lattice_vectors[0,:] * data.scale / data.Na
        self.gridVec[1,:] = data.lattice_vectors[1,:] * data.scale / data.Nb
        self.gridVec[2,:] = data.lattice_vectors[2,:] * data.scale / data.Nc
        
        a, magn = listCartesian( data.lattice_vectors[0,:], data.scale, Rc, data.Na)
        
        
        # 4) Calculate the r' values for p = 0 and all previously 
        # generated data points from step 3 (cartesian function)
        # based on http://stackoverflow.com/questions/8251111/collecting-results-from-a-loop-that-returns-numpy-arrays
        print("{} > GettingStarted > Calculating r'".format(dt.now()))      

        #Counter: keep track of the amount of raccents
        # Define as array to get pass by reference
        # Define as integer as this is what Fortran expects
        counter = np.empty((1,), order='F', dtype=np.int32)
        # Total size of the input
        #totalrange = (NboxA * data.Na + 1) * (NboxB * data.Nb + 1) * (NboxC * data.Nc + 1)
        totalrange = np.size(a)**3
        print(totalrange, NboxA)
        # Make arrays
        retraccent = np.empty((totalrange,), order='F') # Output r' (to be cut later)
        retarraysIJK = np.empty((totalrange,3), order='F') # Output IJK (to be cut later)
        
        print("{} > GettingStarted > Calculating r' > Fortran Starting".format(dt.now()))
        # SUBROUTINE RACCENTCALC(NBOXA,NBOXB,NBOXC,NA,NB,NC,GRIDVEC,CUTOFF,RETRACCENT,RETARRAYIJK,NUMR)
        pp.raccentcalc(NboxA, NboxB, NboxC, data.Na, data.Nb, data.Nc, self.gridVec, Rc, retraccent, retarraysIJK, counter) 
        print("{} > GettingStarted > Calculating r' > Fortran Exiting".format(dt.now()))
        # Fetch the cutoff index from the array
        cutindex = counter[0]

        cutraccent = np.array(retraccent[0:cutindex])
        cutarraysIJK = np.array(retarraysIJK[0:cutindex,:])
        
        print(cutindex, cutraccent.size, np.size(cutarraysIJK,0), np.size(cutarraysIJK,1))

        # TODO: The old way. Maybe in the UT???
        #cutraccent, cutindex, cutarraysIJK = raccentCalc(arraysIJK, self.gridVec, Rc)
        
        
        # 5) Sort all the arrays test
        print("{} > GettingStarted > Sorting arrays".format(dt.now()))
        raccentSorted, arraysIJKSorted  = arraySort(cutraccent, cutarraysIJK)
        
        # 6) Create an array for the n(ijk) values - test
        print("{} > GettingStarted > Sorting n".format(dt.now()))

        # SUBROUTINE NASSIGN(ASIZE,ARRAYSIJK,NARRAY,P,N)
        """
        arraysnSorted = np.empty((cutindex,), order='F')
        p = [24, 6, 0]
        pp.nassign(arraysIJKSorted, data.nijk, p, arraysnSorted)
        """
        arraysnSorted = None
        
        # Save the generated data in the object
        # Change names here please :)
        print("{} > GettingStarted > Saving data".format(dt.now()))
        self.vol = vol
        self.IJK = arraysIJKSorted
        self.raccent = raccentSorted
        # Deprecated: calculate on a per p basis
        self.n = arraysnSorted
        self.POS_RCUT = cutindex
        
        self.size_n = [data.Na, data.Nb, data.Nc]
        
        print("{} > GettingStarted > Exiting".format(dt.now()))
        
    def doNAssign(self, p, nijk):
        arraysnSorted = np.empty((self.POS_RCUT,), order='F')
        pp.nassign(self.IJK, nijk, p, arraysnSorted)
        
        return arraysnSorted

"""
=====================================================================================
Functies (origineel uit module_setup.py)
=====================================================================================
"""

# 1) Defining the cut-off value 
def cutoffchoice(default=2):
    """
    This function expects an input parameter "y" or "n".
    "y" means that the cut-off will be manually chosen.
    "n" means that the cut-off will be calculated as using the below calculations
    
    ML NOTE: the prompt from compute_a has been moved into this function.
    This is now a freestanding thing (aka more elegant)
    """
    
    prompt = "Do you want to manually insert the cut-off value? (y/n)\n"
    x = input(prompt)
    
    if x == "y":
        prompt = "Insert the cut-off value\n"
        covalue = input(prompt)
        print("The cutt-off value is set to Rc={}".format(float(covalue)))
        return float(covalue)
    elif x == "n":
        print("A code is still missing, run program again and press y.")
        # ML
        print("A default value of {} is used!".format(default))
        return(default)
    else:
        print("Input value is not y or n, please run program again.")
        
# 2) Generate arrays for input cartesian function
def listCartesian(vector, scale, cutoff, N):
    """
    This function determines the amount of boxes that will be listed in an array (I J K values), 
    based on the cut-off. The resulting array will be the input array for the cartesian function.
    :param vector: the unit vector of the lattice
    :param scale:  the scaling factor for all unit vectors
    :param cutoff: the cut-off value selected or calculated
    :param N: # grids in one box
    """
    # Step1: calculate the magnitude of a, b and c - assumed that the unit vectors are 0.5 or -0.5
    #         and that the magnitude of the three vectors are equal
    magn = math.sqrt(sum(vector)**2)*scale
    #Just snejking around
    # Step2: calculate the amount of boxes that fall in the cut-off range
    Nbox = math.ceil(cutoff/magn)

    # Step3: determine the upper and lower range
    lRange = -Nbox*N     
    uRange = Nbox*N
    array = np.linspace(uRange,lRange,num=uRange+uRange+1)  
    
    return array, magn
    

# 3) Generate arrays for i, j and k
    # from http://stackoverflow.com/questions/1208118/using-numpy-to-build-an-array-of-all-combinations-of-two-arrays
    # from https://github.com/scikit-learn/scikit-learn/blob/master/sklearn/utils/extmath.py

def cartesian(arrays, out=None):
    """
    Takes in arrays and generates all possible combinations
    :param arrays: the input arrays
    """
    arrays = [np.asarray(x) for x in arrays]
    shape = (len(x) for x in arrays)
    dtype = arrays[0].dtype

    ix = np.indices(shape)
    ix = ix.reshape(len(arrays), -1).T

    if out is None:
        out = np.empty_like(ix, dtype=dtype)

    for n, arr in enumerate(arrays):
        out[:, n] = arrays[n][ix[:, n]]

    return out


# 4) Calculate the r" values for p = 0 and all previously 
# generated data points from step 3 (cartesian function)       
def raccentCalc(arraysIJK, gridVec, cutoff):
    """
    raccentSort calculates all r' values corresponding to the IJK arrays (and taken p = 0)
    This uses the input of the cartesian funciton
    """
  
    vecA = gridVec[0,:]
    vecB = gridVec[1,:]
    vecC = gridVec[2,:]
    
    #hehehe eens zien wie er goed oplet :P xoxox C.
    
    totalrange = np.size(arraysIJK, 0)
    raccent = np.empty(totalrange) 
    RcRc = cutoff*cutoff
    counter = 0
    cutarrayIJK = np.empty((totalrange,3))
    
    for num in range(0,totalrange):
        if num % 10**6 == 0:
            print('{} num {}'.format(dt.now(), num))
        pos = vecA * arraysIJK[num, 0] + vecB * arraysIJK[num, 1] + vecC * arraysIJK[num, 2]
        posdot = np.dot(pos,pos)
        if posdot <= RcRc:
            raccent[counter] = math.sqrt(posdot)
            cutarrayIJK[counter,:] = arraysIJK[num,:]
            counter = counter + 1 
        
    print('arrayrange {}'.format(counter))  
    cutraccent = np.resize(raccent,counter)
    print(np.size(cutraccent,0))      
    
    return cutraccent, counter, cutarrayIJK

# 5) Sort all the arrays
def arraySort(arrSource, array):
    """
    arraySort sorts takes the indices that represent a sort of the arrSource through np.argsort
    and sorts both the arrSource and another input array.
    :param arrSource: the source array
    :param array: the array that will be sorted based on  the indices of the source array
    """
    order = np.argsort(arrSource)
    arrSSorted =  np.array(arrSource, order='F')[order]
    arraySorted =   np.array(array, order='F')[order]
    
    return arrSSorted, arraySorted

# 6) Create an array for the n(ijk) values
# SNEJK was here C.
def nAssign(arraysIJK,narray):
    """
    A function that assigns the n values to the corresponding I,J and K indices
    :param arraysIJK: the array that contains the indices per row
    :param narray: the array that contains all the n values
    """
    totalrange = np.size(arraysIJK,0)
    n = np.empty(totalrange, order='F')
    nrangeI = np.size(narray,0)
    nrangeJ = np.size(narray,1)
    nrangeK = np.size(narray,2)
        
    for num in range(0,totalrange):
        ixI = arraysIJK[num, 0]
        ixJ = arraysIJK[num, 1]
        ixK = arraysIJK[num, 2]
        ixI = ixI - math.floor(float(ixI)/float(nrangeI))*nrangeI
        ixJ = ixJ - math.floor(float(ixJ)/float(nrangeJ))*nrangeJ
        ixK = ixK - math.floor(float(ixK)/float(nrangeK))*nrangeK
        n[num] = narray[int(ixI),int(ixJ),int(ixK)]
        #print("%8i %2i %2i %2i %+e" % (num,ixI,ixJ,ixK, n[num]))
        #tralalalalal C.
    
    return n
